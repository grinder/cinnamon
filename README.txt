Versão do Cinnamon : 5.4x

Requerimentos:
- slackware64 15.0
- pacote de audio - http://www.gitlab.com/grinder/audio.git

Essa minha versão do Cinnamon é baseada na versão do Willysr
https://github.com/willysr/csb
***Todos os créditos vão para ele.

Esse source foi construído principalmente para quem quiser usar a última versão
do Cinnamon

Conteúdo:
- evince
- eye of gnome
- file roller
- meld
- gedit
- gnome terminal
- gnome calculator
- gnome screenshot
- gnome system monitor
- gthumb
- nemo extensions (fileroller,share,audio-tab,terminal,compare,python)

Como instalar:
- você precisa baixar o .zip ou clonar o git e então executar o script
sh build-cinnamon.sh

------------------------------------------------------------------------------

Cinnamon Version : 5.4x
Cinnamon Source : github

Requirements:
- slackware64 15.0
- audio package - http://www.gitlab.com/grinder/audio.git

This my version of Cinnamon is based on the version of Willysr
https://github.com/willysr/csb
*** All credits go to him.

This source was built primarily for those who want to use the latest Cinnamon
version

Contents:
- evince
- eye of gnome
- file roller
- meld
- gedit
- gnome terminal
- gnome calculator
- gnome screenshot
- gnome system monitor
- gthumb
- nemo extensions (fileroller,share,audio-tab,terminal,compare,python)

How to install:
- you need to download the .zip or clone git and then run the script
sh build-cinnamon.sh
